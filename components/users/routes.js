module.exports = (app) => {
  const users = require('./controller');
  var router = require('express').Router();

  router.post('/register', users.create);
  router.get('/findAll', users.findAll);
  router.get('/findAllOnline', users.findAllOnline);
  router.get('/findUser/:id', users.findOne);
  router.put('/update/:id', users.update);
  router.delete('/delete/:id', users.delete);
  app.use('/api/users', router);
};
